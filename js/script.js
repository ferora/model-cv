/*
 * Activer les liens de navigation
 */
var ulNav = document.getElementById('navigation');

ulNav.addEventListener('click', activeLink);

function activeLink(e) {
    var lis = e.currentTarget.children;

    var i;
    for (i = 0; i < lis.length; i++) {
        lis[i].firstChild.className = 'default';
    }

    e.target.className = 'active';
}

/*
 * Code Konami
 */
document.getElementById('body').addEventListener('keydown', konamiCode);

var home = document.getElementsByTagName('nav')[0];
var backHome = home.style.backgroundColor;

var konami = [38, 38, 40, 40, 37, 39, 37, 39, 16, 66, 16, 65, 13];
var j=0;

function konamiCode(e) {
    //alert(e.keyCode);
    //alert(konami[j]);

    if ( e.keyCode == konami[j] ) {
        j++;
        if ( j == konami.length ) {
            j = 0;
            home.style.backgroundColor = 'green';
            setTimeout(function() {home.style.backgroundColor = backHome;}, 3000);
        }
        else {
            home.style.backgroundColor = 'blue';
            setTimeout(function() {j=0; home.style.backgroundColor = backHome;}, 10000);
        }
    }
    else if ( j > 0 ) {
        j = 0;
        home.style.backgroundColor = 'red';
        setTimeout(function() {home.style.backgroundColor = backHome;}, 3000);
    }
}

/*
 * Navigation douce
 */
$('a[href*="#"]').click(function() {
    var id = $(this).attr('href');

    // Coordonnée y où scroller -> $(id).offset().top
    $('html, body').animate({scrollTop: $(id).offset().top - 50}, 1500);
});

/*
 * Header sticky
 */
$(window).scroll(function(){
    var sticky = $('header');
    var scroll = $(window).scrollTop();

    if (scroll >= 100)
        sticky.addClass('fixed');
    else
        sticky.removeClass('fixed');
});

/*
 * Filtrage du portfolio
 */
var selectedClass = "";

$('.filtre').click(function(){
    selectedClass = $(this).attr('id').replace('#','');

    $('#galerie').fadeTo(100, 0.1);
    $('#galerie > div').not("." + selectedClass).fadeOut().removeClass('animation');

    setTimeout(function(){
       $('.' + selectedClass).fadeIn().addClass('animation');
       $('#galerie').fadeTo(300, 2);
    }, 300);
});

/*
 * Diaporama
 * origine : https://kenwheeler.github.io/slick/
 */
$(document).ready(function(){
    $('#diaporama').slick({
        infinite: true,
        speed: 500,
        fade: true,
        cssEase: 'linear'
    });
});
